{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import Data.Function ((&))
import Control.Monad (ap, liftM)
import Control.Monad.IO.Class
data IndentationSetting
  = Spaces Int
  | Tabs

data Env = Env
  { indentationSetting :: IndentationSetting
  , exceptionHandler :: Exception -> IO ()
  , logger :: String -> IO ()
  }

getIndentationSetting :: IO IndentationSetting
getIndentationSetting = return Tabs

indented :: String -> IndentationSettingM String
-- indented string (Spaces n) = replicate n ' ' ++ string
-- indented string Tabs = '\t' : string
indented string = do
  iSetting <- ask
  return $ case iSetting of
    (Spaces n) -> replicate n ' ' ++ string
    Tabs -> '\t' : string

indentTwice :: String -> IndentationSettingM String
indentTwice string = do
  indentedOnce <- indented string
  indented indentedOnce

main1 :: IO ()
main1 = do
  indentationSetting <- getIndentationSetting
  flip runIndentationSettingT indentationSetting $ do
    indentedTwice <- indentTwice "Hello World"
    liftIO $ putStrLn indentedTwice
    (liftIO . putStrLn) =<< indented "¡Hola!"

type IndentationSettingM = IndentationSettingT IO

newtype IndentationSettingT m a = IndentationSettingT
  { runIndentationSettingT :: IndentationSetting -> m a }

ask :: Monad m => IndentationSettingT m IndentationSetting
ask = IndentationSettingT return

instance Monad m => Functor (IndentationSettingT m) where
  fmap = liftM

instance Monad m => Applicative (IndentationSettingT m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (IndentationSettingT m) where
  return a = IndentationSettingT (const $ return a)
  IndentationSettingT action >>= cont = IndentationSettingT
    -- { runIndentationSettingM = \iSetting -> iSetting
    --     & action
    --     & cont
    --     & runIndentationSettingM
    --     & ($ iSetting)
    { runIndentationSettingT = \iSetting -> do
        a <- action iSetting
        let bAction = cont a
        runIndentationSettingT bAction iSetting
    }

instance MonadIO IndentationSettingM where
  liftIO ia = IndentationSettingT (const ia)

data Exception = DatAberNichSchoen

data Either' a b = Left' a | Right' b

type ExceptM = ExceptT IO

newtype ExceptT m a = ExceptT
  { runExceptT :: m (Either Exception a) }

throw :: Monad m => Exception -> ExceptT m a
throw exception = ExceptT
  { runExceptT = return $ Left exception }

catch :: Monad m => ExceptT m a -> (Exception -> m a) -> m a
catch ExceptT { runExceptT } handler = do
  result <- runExceptT
  case result of
    Left e -> handler e
    Right a -> return a

indentNoEvilWord :: Monad m => String -> ExceptT m String
indentNoEvilWord "böse" = throw DatAberNichSchoen
indentNoEvilWord string = error "Geht leider nicht" -- indented string

instance Functor (ExceptT m) where
instance Applicative (ExceptT m) where
instance Monad m => Monad (ExceptT m) where
  return :: a -> (ExceptT m) a
  return a = ExceptT $ return $ Right a

  (>>=) :: (ExceptT m) a -> (a -> (ExceptT m) b) -> (ExceptT m) b
  ExceptT action >>= cont = ExceptT $ do
    result <- action
    case result of
      Left e -> return $ Left e
      Right a -> runExceptT $ cont a

functionToTuple :: (Bool -> a) -> (a, a)
functionToTuple f = (f False, f True)

tupleToFunction :: (a, a) -> (Bool -> a)
tupleToFunction (fFalse, fTrue) = \bool -> case bool of
  False -> fFalse
  True -> fTrue

data Stream a = Stream
  { head :: a
  , tail :: Stream a
  }

newtype Monster a = Monster
  { runMonster :: IndentationSetting -> IO (Either Exception a) }

-- instance Monad
--   (>>=) = unleserlich groß

liftI :: IndentationSettingM a -> Monster a
liftI action = Monster
  { runMonster = fmap Right . runIndentationSettingT action }

-- Keine Monade!!
type Monster2 a = ExceptM (IndentationSettingM a)
  -- = IO (Either Except (IndentationSettingM a))
  -- = IO (Either Except (IndentationSetting -> IO a))

-- kostenlose Monade
type Stack a = ExceptT IndentationSettingM a
  -- = IndentationSettingM (Either Exception a)
  -- = IndentationSetting -> IO (Either Exception a)

type Stack2 a = IndentationSettingT ExceptM a
  -- = IndentationSetting -> ExceptM a
  -- = IndentationSetting -> IO (Either Exception a)

newtype LoggerT m a = LoggerT
  { runLoggerT :: m (a, [String]) }

type LoggerM = LoggerT IO

instance Monad m => Functor (LoggerT m) where
instance Monad m => Applicative (LoggerT m) where
instance Monad m => Monad (LoggerT m) where
  return = _
  (>>=) = _

instance MonadIO m => MonadIO (LoggerT m) where

type Stack3 a = LoggerT ExceptM a
  -- = ExceptM (a, [String])
  -- = IO (Either Exception (a, [String]))

type Stack4 a = ExceptT LoggerM a
  -- = LoggerM (Either Exception a)
  -- = IO (Either Exception a, [String])

runLoggerIO :: MonadIO m => LoggerT m a -> m a
runLoggerIO LoggerT { runLoggerT } = do
  (result, log) <- runLoggerT
  liftIO $ print log -- Fancy file handling
  return result

runExceptIO :: MonadIO m => ExceptT m a -> m a
runExceptIO action = catch action $ error "Keine Ahnung"

runStack3 :: Stack3 a -> IO a
runStack3 = runExceptIO . runLoggerIO

runStack4 :: Stack4 a -> IO a
runStack4 = runLoggerIO . runExceptIO



main :: IO ()
main = main1
