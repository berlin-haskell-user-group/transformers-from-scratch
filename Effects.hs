{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ConstraintKinds #-}
import Control.Exception (throw, AssertionFailed (AssertionFailed))
import Control.Monad (liftM, ap)
import Data.Functor.Identity (Identity(runIdentity, Identity))
import Distribution.Fields.Lexer (Token(Indent))

getIndentationIO :: IO Int
getIndentationIO = return 2

throwAssertionFailed :: String -> IO ()
throwAssertionFailed = throw . AssertionFailed

verifyIndentation :: Monad m => Int -> Eff IndentationEffect m ()
verifyIndentation 4 = Effect $ E2 $ Throw "nope"
verifyIndentation _ = return ()

-- core :: (Has IndentationSettings m, Has (Error String) m) => m ()
core :: Monad m => Eff IndentationEffect m ()
core = do
  indentation <- Effect $ E1 GetIndentation
  verifyIndentation indentation
  return ()

{-
instance Has IndentationSettings IO where
  getIndentation = readFile ...

instance Has (Error String) IO where
  ...

instance Has (Error String) (Either String) where
  throwAssertionFailed = Left
-}


-- Type class variant
class MonadGetIndentation m where
  getMyIndentation :: m Int

class MonadThrow m where
  throwMy :: String -> m ()

type MonadMyApp m = (MonadGetIndentation m, MonadThrow m)

instance MonadGetIndentation IO where
  getMyIndentation = getIndentationIO

instance MonadThrow IO where
  throwMy = throwAssertionFailed

verifyMyIndentation :: (Monad m, MonadThrow m) => Int -> m ()
verifyMyIndentation 4 = throwMy "nope"
verifyMyIndentation _ = return ()

coreWithTypeClass :: (Monad m, MonadMyApp m) => m ()
coreWithTypeClass = do
  indentation <-getMyIndentation
  verifyMyIndentation indentation
  return ()

handlerGetIndentationIO :: GetIndentationEffect a -> IO a
handlerGetIndentationIO GetIndentation = getIndentationIO

handlerThrowIO :: ThrowEffect a -> IO a
handlerThrowIO (Throw msg) = throwAssertionFailed msg

handlerIO :: IndentationEffect a -> IO a
handlerIO = handlerGetIndentationIO |+| handlerThrowIO

handlerIdentity :: IndentationEffect a -> Identity a
handlerIdentity (E1 GetIndentation) = Identity 2
handlerIdentity (E2 (Throw _)) = return ()

handlerEither :: IndentationEffect a -> Either String a
handlerEither (E1 GetIndentation) = Right 2
handlerEither (E2 (Throw msg)) = Left msg

handlerEitherFunction :: IndentationEffect a -> (Int -> Either String a)
handlerEitherFunction (E1 GetIndentation) = \indentation -> Right indentation
handlerEitherFunction (E2 (Throw msg)) = const $ Left msg

main = do
  runEff handlerIO core
  putStrLn "hi"

test :: ()
test = runIdentity $ runEff handlerIdentity core

testEither :: Either String ()
testEither = runEff handlerEither core

{-
Some libraries:

* fused-effects
* polysemy, freer, operational
* rio, effects

-}

data Eff e m a where
  Lift :: m a -> Eff e m a

  -- (>>=) :: Monad m => m a -> (a -> m b) -> m b
  (:>>=) :: Eff e m a -> (a -> Eff e m b) -> Eff e m b
  Effect :: e a -> Eff e m a

data (:+:) e1 e2 a = E1 (e1 a) | E2 (e2 a)

(|+|) :: (e1 a -> m a) -> (e2 a -> m a) -> (e1 :+: e2) a -> m a
handler1 |+| handler2 = \case
  E1 e1 -> handler1 e1
  E2 e2 -> handler2 e2

data GetIndentationEffect a where
  GetIndentation :: GetIndentationEffect Int

data ThrowEffect a where
  Throw :: String -> ThrowEffect ()

type IndentationEffect
  =   GetIndentationEffect
  :+: ThrowEffect


instance Monad m => Functor (Eff e m) where
  fmap = liftM
instance Monad m => Applicative (Eff e m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (Eff e m) where
  return = Lift . return
  (>>=) = (:>>=)

runEff :: Monad m => (forall x . e x -> m x) -> Eff e m a -> m a
runEff _ (Lift action) = action
runEff handler (action :>>= function) = runEff handler action >>= (runEff handler . function)
runEff handler (Effect effect) = handler effect
