{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE FlexibleContexts #-}
import Control.Monad (liftM, ap)
import Data.Void
import Text.Read (readMaybe)
import Data.Function ((&))

main = print 3

newtype ReaderT r m a = ReaderT { runReaderT :: r -> m a }

simply3 :: Monad m => ReaderT r m Integer
simply3 = ReaderT $ const $ return 3

ask :: Monad m => ReaderT r m r
ask = ReaderT return

asks :: Monad m => (r -> a) -> ReaderT r m a
asks f = ReaderT $ return . f

data FooConf = FooConf
  { foo1 :: Int
  , foo2 :: String
  } deriving (Read, Show)

askFoo1 :: Monad m => ReaderT FooConf m Int
askFoo1 = asks foo1

instance Functor m => Functor (ReaderT r m) where
  fmap f (ReaderT ra) = ReaderT $ fmap f . ra

instance Monad m => Applicative (ReaderT r m) where
  pure = ReaderT . const . pure
  (<*>) = ap

instance Monad m => Monad (ReaderT r m) where
  ReaderT ra >>= f = ReaderT $ \r ->
    do
      a <- ra r
      runReaderT (f a) r

showConf :: Monad m => ReaderT FooConf m String
showConf = do
  myFoo1 <- asks foo1
  myFoo2 <- asks foo2
  return $ "foo1 is " ++ show myFoo1 ++ " and foo2 is " ++ myFoo2

liftReaderT :: m a -> ReaderT r m a
liftReaderT = ReaderT . const

debugConf :: ReaderT FooConf IO ()
debugConf = do
  myFoo1 <- asks foo1
  liftReaderT $ print myFoo1

showConfDebug :: ReaderT FooConf IO String
showConfDebug = debugConf >> showConf

interactiveConf :: IO ()
interactiveConf = do
  putStrLn "Please enter config"
  conf <- readLn
  string <- runReaderT showConfDebug conf
  putStrLn string

newtype ExceptT e m a = ExceptT { runExceptT :: m (Either e a) }

throw :: Monad m => e -> ExceptT e m a
throw = ExceptT . return . Left

instance Functor m => Functor (ExceptT e m) where
  fmap f (ExceptT ema) = ExceptT $ fmap (fmap f) ema

instance Monad m => Applicative (ExceptT e m) where
  pure a = ExceptT $ return $ return a
  (<*>) = ap

instance Monad m => Monad (ExceptT e m) where
  ExceptT ema >>= f = ExceptT $ do
    ea <- ema
    case ea of
      Left e  -> return $ Left e
      Right a -> runExceptT $ f a

liftExceptT :: Functor m => m a -> ExceptT e m a
liftExceptT = ExceptT . fmap return

class MonadTrans t where
  lift :: m a -> t m a

instance MonadTrans (ReaderT r) where
  lift = liftReaderT

instance MonadTrans (ExceptT e) where
  lift = liftExceptT

data ParseError = ParseError
  deriving Show

readE :: (Monad m, Read a) => String -> ExceptT ParseError m a
readE string = maybe (throw ParseError) return $ readMaybe string

parseFooConf :: Monad m => String -> ExceptT ParseError m FooConf
parseFooConf string = do
  let (theInt, theString) = break (== ' ') string
  myFoo1 <- readE theInt
  return $ FooConf myFoo1 theString

data ValidationError = ValidationError
  deriving Show

validateFooConf :: Monad m => FooConf -> ExceptT ValidationError m FooConf
validateFooConf fooConf = if length (foo2 fooConf) > 10
    then throw ValidationError
    else return fooConf

catch :: Monad m => ExceptT e1 m a -> (e1 -> ExceptT e2 m a) -> ExceptT e2 m a
catch ema handler = do
  ea <- liftExceptT $ runExceptT ema
  case ea of
    Left e1 -> handler e1
    Right a -> return a

newtype CatchT a m e = CatchT { runCatchT :: ExceptT e m a }

instance Monad m => Functor (CatchT a m) where
  fmap = liftM

instance Monad m => Applicative (CatchT a m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (CatchT a m) where
  return e = CatchT $ throw e
  CatchT e1 >>= f = CatchT $ catch e1 $ runCatchT . f

safe :: Monad m => ExceptT Void m a -> m a
safe ema = do
  ea <- runExceptT ema
  case ea of
    Left void -> absurd void
    Right a -> return a

catchSimple :: Monad m => ExceptT e m a -> (e -> m a) -> m a
catchSimple ema handler = safe $ catch ema $ liftExceptT . handler

myProg :: CatchT FooConf IO String
myProg = do
  ParseError <- CatchT $ parseFooConf =<< liftExceptT getLine
  ValidationError <- CatchT $ validateFooConf $ FooConf 23 "foooo"
  return "Sorry, everything broke"

-- ExceptT e (ReaderT r m) a
-- = (ReaderT r m) (Either e a)
-- = r -> m (Either e a)

-- ReaderT r (ExceptT e m) a
-- = r -> (ExceptT e m) a
-- = r -> m (Either e a)

-- StateT s (ExceptT e m) a
-- = s -> (ExceptT e m) (s, a)
-- = s -> m (Either e (s, a))

-- ExceptT e (StateT s m) a
-- = (StateT s m) (Either e a)
-- = s -> m (s, Either e a)

data ProgramT i m a where
  Lift :: m a -> ProgramT i m a
  Instruction :: i a -> ProgramT i m a
  Bind :: ProgramT i m a -> (a -> ProgramT i m b) -> ProgramT i m b

instance Monad m => Functor (ProgramT i m) where
  fmap = liftM

instance Monad m => Applicative (ProgramT i m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (ProgramT i m) where
  return = Lift . return
  (>>=) = Bind

instance MonadTrans (ProgramT i) where
  lift = Lift

mapI :: (forall x . i1 x -> i2 x) -> ProgramT i1 m a -> ProgramT i2 m a
mapI _ (Lift ma) = Lift ma
mapI morphism (Instruction i1a) = Instruction $ morphism i1a
mapI morphism (Bind pi1ma fapi1ma) = Bind (mapI morphism pi1ma) (mapI morphism . fapi1ma)

newtype IdentityT m a = IdentityT { runIdentityT :: m a }
  deriving (Functor, Applicative, Monad)

instance MonadTrans IdentityT where
  lift = IdentityT

interpret :: Monad m => (forall x . i x -> m x) -> ProgramT i m a -> m a
interpret morphism = runIdentityT . interpretT (IdentityT . morphism)

interpretT :: (Monad m, MonadTrans t, Monad (t m)) => (forall x . i x -> t m x) -> ProgramT i m a -> t m a
interpretT _ (Lift ma) = lift ma
interpretT morphism (Instruction ia) = morphism ia
interpretT morphism (Bind pima fapima) = interpretT morphism pima >>= (interpretT morphism . fapima)

data IList (instructions :: [* -> *]) a where
  Here :: i a -> IList (i ': is) a
  There :: IList is a -> IList (i ': is) a

commute :: IList (i1 ': i2 ': is) a -> IList (i2 ': i1 ': is) a
commute (Here ia) = There $ Here ia
commute (There (Here ia)) = Here ia
commute (There (There iList)) = There $ There iList

inThere :: (IList is a -> IList js a) -> IList (i ': is) a -> IList (i ': js) a
inThere _ (Here ia) = Here ia
inThere f (There iList) = There $ f iList

type family Rest i is :: [* -> *] where
  Rest i (i ': is) = is
  Rest i (j ': is) = j ': Rest i is
  Rest i '[] = '[]

class Member i is where
  inject :: i a -> IList is a
  -- bubble :: IList is a -> IList (i ': Rest i is) a

instance Member i (i ': is) where
  inject = Here
  -- bubble = id

data Proxy (k :: kind) = Proxy

hereAt :: Proxy is -> IList (i ': is) a -> IList (i ': is) a
hereAt _ (Here i) = Here i
hereAt _ (There i) = There i

thereAt :: Proxy i -> IList (i ': is) a -> IList (i ': is) a
thereAt _ (Here i) = Here i
thereAt _ (There i) = There i

instance Member i is => Member i (j ': is) where
  inject = There . inject

  -- bubble :: IList (j ': is) a -> IList (i ': Rest i (j ': is)) a
  -- bubble (Here ja) = There $ Here ja
  -- bubble (Here ja) = _
  -- bubble (There instr) = There $ bubble instr

instructionM :: Member i is => i a -> ProgramT (IList is) m a
instructionM = Instruction . inject

handleHere :: Monad m => (forall x . i x -> m x) -> ProgramT (IList (i ': is)) m a -> ProgramT (IList is) m a
handleHere morphism = runIdentityT . handleHereT (IdentityT . morphism)

class MFunctor t where
  hoist :: (forall x . m x -> n x) -> t m a -> t n a

instance MFunctor (ReaderT r) where
  hoist morphism (ReaderT frma) = ReaderT $ morphism . frma

instance MFunctor IdentityT where
  hoist morphism = IdentityT . morphism . runIdentityT

instance MFunctor (ExceptT e) where
  hoist morphism = ExceptT . morphism . runExceptT

instance MFunctor (ProgramT i) where
  hoist morphism (Lift ma) = Lift $ morphism ma
  hoist _        (Instruction ia) = Instruction ia
  hoist morphism (Bind pima fapima) = Bind (hoist morphism pima) (hoist morphism . fapima)

handleHereT :: (MonadTrans t, MFunctor t, Monad (t (ProgramT (IList is) m))) => (forall x . i x -> t m x) -> ProgramT (IList (i ': is)) m a -> t (ProgramT (IList is) m) a
handleHereT _ (Lift ma) = lift $ lift ma
handleHereT morphism (Instruction (Here instruction)) = instruction
  & morphism
  & hoist lift
handleHereT morphism (Instruction (There instruction)) = lift $ Instruction instruction
handleHereT morphism (Bind action continuation) = handleHereT morphism action >>= (handleHereT morphism . continuation)

done :: Monad m => ProgramT (IList '[]) m a -> m a
done (Lift ma) = ma
done (Bind action continuation) = done action >>= (done . continuation)

newtype ThrowI e a = ThrowI { unThrowI :: e }

throwI :: Member (ThrowI e) is => e -> ProgramT (IList is) m a
throwI = instructionM . ThrowI

handleThrow :: Monad m => ProgramT (IList (ThrowI e ': is)) m a -> ExceptT e (ProgramT (IList is) m) a
handleThrow = handleHereT $ ExceptT . return . Left . unThrowI

handleThrow' :: Monad m => ProgramT (IList (ThrowI e ': is)) m a -> (ProgramT (IList is) m) (Either e a)
handleThrow' = runExceptT . handleThrow
